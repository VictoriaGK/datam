import sys
import psycopg2
import numpy as np
import psycopg2
from psycopg2 import sql
from psycopg2.sql import SQL, Identifier

conn = psycopg2.connect(dbname='', user='postgres',
                        password='postgres', host='database-1-instance-1.c7ywad42wtjs.us-east-1.rds.amazonaws.com', port=5432)
cursor = conn.cursor()
conn.autocommit = True




def get_roots(cursor):
    cursor.execute("SELECT source FROM graph UNION SELECT dest FROM graph;")
    rows = cursor.fetchall()
    roots = []
    for row in rows:
        roots.append(row[0])
    return roots

def get_bad_roots(cursor):
    cursor.execute("SELECT dest FROM graph EXCEPT SELECT source FROM graph;")
    rows = cursor.fetchall()
    roots = []
    for row in rows:
        roots.append(row[0])
    return roots

def get_good_roots(cursor):
    cursor.execute("SELECT distinct source FROM graph;")
    rows = cursor.fetchall()
    roots = []
    for row in rows:
        roots.append(row[0])
    return roots


def make_graphs(cursor, roots):
    graphs = np.zeros((len(roots), len(roots)))
    for root in roots:

        # create = 'SELECT dest FROM graph WHERE source = {} ;'.format(root)
        cursor.execute("SELECT dest FROM graph WHERE source = '%s' EXCEPT (SELECT dest FROM graph EXCEPT SELECT source FROM graph) ;" % root)
        # a = list()
        # a.append(root)
        # print(root)
        #
        # print(a)
        # create = sql.SQL("SELECT dest FROM graph WHERE source = %s;", a)

        # cursor.execute("SELECT dest FROM graph WHERE source = %s;", a)
        # create = sql.SQL('SELECT dest FROM graph WHERE source = {};')

        # cursor.execute(create)
        # print(create.format(sql.Identifier(a)).as_string(conn))
        rows = cursor.fetchall()
        for row in rows:
            graphs[roots.index(row[0]), roots.index(root)]= 1 / len(rows)

        # if root == "E":
        #     for root2 in roots:
        #         # print(graphs[roots.index(root2),roots.index(root)])
        #         print(graphs[roots.index(root), roots.index(root2)])
        #     print(graphs)
    return graphs


def rank_bad_roots(cursor, vector, ):
    a = []
    for root in bad_roots:
        roots = get_good_roots(cursor)
        v0 = np.zeros((len(roots), 1))

        for root2 in roots:
            cursor.execute("SELECT dest FROM graph WHERE source = '%s';" % root2)
            rows = cursor.fetchall()
            for row in rows:
                if row[0]==root:
                    v0[roots.index(root2)] = 1 / len(rows)
        b = sum(v0*vector)
        a.append(b)
    return a


roots = get_good_roots(cursor)
bad_roots = get_bad_roots(cursor)
graph = make_graphs(cursor, roots)
v0 = np.zeros((len(roots), 1))

for i in range(len(roots)):
    v0[i, 0] = 1 / len(roots)

for i in range(20):
    v0 = np.dot(graph, v0)



v1 = np.array(rank_bad_roots(cursor, v0))
if  len(v1)>0:
    v0 = np.row_stack((v0, v1))
    roots += bad_roots
page_ranks = {roots[i]:v0[i, 0] for i in range(len(roots))}
print(page_ranks)