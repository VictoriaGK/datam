centers = [(6, 8)]
roots = [(2,2), (12,6), (4,10), (9,3), (3,4), (5,2), (10,5), (11,4), (12,3), (4,8), (6,8), (7,10)]

dist = {i:0 for i in roots}


def k_init(roots,centers, k):
    dist = {root: (((root[0] - centers[0][0]) ** 2 + (root[1] - centers[0][1]) ** 2) ** (1 / 2)) for root in roots}
    for key, d in dist.items():
        if d == max(dist.values()):
            centers.append(key)
    while len(centers)<=k:
        for root in roots:
            dist[root] = min(dist[root],(((root[0]-centers[-1][0])**2 + (root[1]-centers[-1][1])**2)**(1/2)))
        for key,d in dist.items():
            if d==max(dist.values()) and (key not in centers):
                centers.append(key)
            if len(centers)==k:
                return centers

# print(k_init(roots,centers,3))


def k_means(centers,roots):
    all = {i: [] for i in centers}
    isNotEq = False
    while not(isNotEq):
        all = {i: [] for i in centers}
        distance = {i: 0 for i in centers}
        allN = {}
        for root in roots:
            for center in centers:
                if center!=root:
                    distance[center] = ((root[0] - center[0]) ** 2 + (root[1] - center[1]) ** 2) ** (1 / 2)
                else:
                    distance[center] = 0
            out = False
            for key, d in distance.items():
                if d == min(distance.values()) and not(out):

                    all[key].append(root)
                    out = True
        for key,val in all.items():
            x = sum([i
                     [0] for i in val])/len(val)
            y = sum([i[1] for i in val])/len(val)
            allN[(x,y)]=[]
        result = [x for x in centers if x in [val for val in allN.keys()]]
        isNotEq = (len(result)==len(centers))
        centers = [val for val in allN.keys()]
    return all



k = 4
a = k_init(roots,centers,k)
print("first 4 centers - ", a)
for key,val in k_means(a,roots).items():
    print("key-  ", key,"  val- ", val )


