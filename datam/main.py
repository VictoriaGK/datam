import operator
import sys
import time

import requests


from psycopg2 import sql
from psycopg2.sql import Identifier, SQL

from settings import token, my_id, api_v
import psycopg2
conn = psycopg2.connect(dbname='data1', user='postgres',
                        password='postgres', host='localhost')
cursor = conn.cursor()
conn.autocommit = True


class VkFriends():
	"""
	Находит друзей
	"""

	def __init__(self, *pargs):
			self.token, self.my_id, self.api_v= pargs

			self.all_friends, self.count_friends = self.friends(self.my_id)


	def request_url(self, method_name, parameters, access_token=False):
		"""read https://vk.com/dev/api_requests"""

		req_url = 'https://api.vk.com/method/{method_name}?{parameters}&v={api_v}'.format(
			method_name=method_name, api_v=self.api_v, parameters=parameters)

		if access_token:
			req_url = '{}&access_token={token}'.format(req_url, token=self.token)

		return req_url


	def friends(self, id):
		"""
		read https://vk.com/dev/friends.get
		Принимает идентификатор пользователя
		"""
		r = requests.get(self.request_url('friends.get',
				'user_id=%s&fields=first_name,last_name' % id, True)).json()['response']
		#r = list(filter((lambda x: 'deactivated' not in x.keys()), r['items']))
		return {item['id']: [item['first_name'],item['last_name'], {"count":0}] for item in r['items']}, r['count']

	def groups(self, id):
		"""
		read https://vk.com/dev/groups.get
		Принимает идентификатор пользователя
		"""
		b = ['Закрытая группа', 'Открытая группа', 'Частная группа', None]
		try:
			r = requests.get(self.request_url('groups.get',
					'user_id=%s&extended=1&fields=name,activity' % id, True)).json()['response']
			time.sleep(0.2)

		except KeyError:
			print()


		a = {}
		try:
			return {item['id']: [item['name'], item.get('activity', None)] for item in r['items'] if item.get('activity', None) not in b}

		except UnboundLocalError:
			r = {'count':None, 'items':None}

			return "net"


	def groups_activity(self, groups , name="table_fr"):
		"""
		read https://vk.com/dev/groups.get
		Принимает список групп
		"""

		cnt = len(groups)
		create = sql.SQL('CREATE TABLE {} (thema VARCHAR , proz DECIMAL)').format(sql.Identifier(name))
		cursor.execute(create)

		if (groups=="net"):
			return {'ban':0}
		a = {}

		for item, val in groups.items():
			try:
				b = a.get(val[1])
				b+=(1/cnt)
				a.update({val[1]: b})
				b = round(b, 7)
				cursor.execute(SQL("UPDATE {} SET proz = %s WHERE thema = %s").format(Identifier(name)), (b, val[1],))

			except Exception:
				b = 1/cnt
				b = round(b, 7)
				cursor.execute(SQL("INSERT INTO {} VALUES (%s,%s)").format(Identifier(name)),(val[1],b, ))
				a.update({val[1]: b})
		return a


if __name__ == '__main__':
	a = VkFriends(token, my_id, api_v)


	def common_groups1(friends,my_id):
		"""
		Принимает список друзей и и формирует список друзей с общими интересами
		"""
		a.groups_activity(a.groups(my_id),'my_name')
		groups_friends = []
		groups_friends_full = {}


		for friend, val in friends[0].items():
			a.groups_activity(a.groups(friend))
			cursor.execute(SQL("SELECT  sum(pr) from ( SELECT my_name.thema,my_name.proz as pr,table_fr.proz from my_name inner join table_fr ON table_fr.thema=my_name.thema AND( (my_name.proz - 0.1 < table_fr.proz )AND (my_name.proz+0.1 > table_fr.proz))) AS foo"))
			records = cursor.fetchone()
			if(records[0]!=None)and(records[0]>0.45):
				groups_friends.append(val[0]+' '+val[1])
			if (records[0]!=None):
				groups_friends_full.update({val[0]+' '+val[1]: float(records[0])})
			cursor.execute(SQL("DROP TABLE table_fr"))

		sorted_x = sorted(groups_friends_full.items(), key=lambda x: x[1], reverse=True)
		return [sorted_x[:5], groups_friends]

	def common_groups(friends):

		"""
		доп метод
		Принимает список друзей и идентификатор пользователя
		формирует список друзей с общими интересами
		"""
		groups_friends = []

		for friend, val in friends[0].items():
			c = 0
			b = 0
			list = a.groups_activity(a.groups(my_id))
			try:
				list2 = a.groups_activity(a.groups(friend))

			except ValueError:
				print()
			for group, val2 in list.items():
				try:
					for gr, v in list2.items():

						if  str(group)==str(gr):
							c +=val2
							b += v
				except KeyError:
					print()
			if c/1.1 <b < 1.1*c:
				groups_friends.append(val[0]+' '+val[1])
		return groups_friends

	print(common_groups1(a.friends(my_id),my_id))
