import csv
import re
import zlib
from itertools import combinations


import pandas
import seaborn
import matplotlib.pyplot as plt


k = set()
f9 = pandas.read_csv('transactions.csv', sep=';')

# print(f9)
for b in f9["BASKET_ID"]:
    k.add(b)


h = {a: [] for a in k}

count1 = {}
for c in f9.iterrows():
    if c[1][0] in count1.keys():
        count1[c[1][0]] +=1
    else:
        count1[c[1][0]] = 1
    h[c[1][1]].append(c[1][0])

count2 = {}


for i in h.keys():
    for j in combinations(h[i], 2):
        f = j[0]+j[1]
        if (f) in count2.keys():
            count2[j[0]+j[1]]+=1
        else:
            count2[j[0] + j[1]] = 1

h1 = {a: 0 for a in range(100)}
for i in count2.keys():
    crc = zlib.crc32(bytes(i.encode())) % 100
    h1[crc]+= count2[i]


bad = []
for i in h1.keys():
    if h1[i]<=2:
        bad.append(i)

count20 = []
for i in count2.keys():
    if zlib.crc32(bytes(i.encode())) % 100 not in bad:
        count20.append(i)

count10 = []
count10bad = []
count2 = {}
for i in count1.keys():
    if count1[i] < 3:
        count10bad.append(i)
for i in count1.keys():
    if count1[i] >= 3:
        count10.append(i)

countall = []
for i in count20:
    fff = []
    for j in count10bad:
        if re.search(j, i)!=None:
            fff.append(re.search(j, i))
    if fff ==[]:
        countall.append(i)

for i in count10:
    countall.append(i)
print(len(countall))