import sys
import psycopg2
import numpy as np
import psycopg2
from psycopg2 import sql
from psycopg2.sql import SQL, Identifier

conn = psycopg2.connect(dbname='postgres', user='postgres',
                        password='postgres', host='localhost', port=5432)
cursor = conn.cursor()
conn.autocommit = True




def get_roots(cursor):
    cursor.execute("SELECT source FROM graph1 UNION SELECT destination FROM graph1;")
    rows = cursor.fetchall()
    roots = []
    for row in rows:
        roots.append(row[0])
    return roots



def make_graphs(cursor, roots):
    graphs = np.zeros((len(roots), len(roots)))
    for root in roots:

        cursor.execute("SELECT source FROM graph1 WHERE destination = '%s';" % root)
        rows = cursor.fetchall()
        for row in rows:
            graphs[roots.index(row[0]), roots.index(root)]= 1
    return graphs

def hits(h,graphs_original):

    graphs_trans = graphs_original.transpose()
    h = np.dot(graphs_trans, h)
    b = [1/max(h)]
    h = b*h
    a = h
    h = np.dot(graphs_original, h)
    b = [1 / max(h)]
    h = b * h
    return h

graphs_original = make_graphs(cursor,get_roots(cursor))
h = np.ones((len(graphs_original[0]), 1))

for i in range(6):
    h = hits(h, graphs_original)
hits_res = {get_roots(cursor)[i]:h[i, 0] for i in range(len(get_roots(cursor)))}
print(hits_res)
graphs_trans = graphs_original.transpose()
h = np.dot(graphs_trans, h)

b = [1/max(h)]
h = b*h
a  = h

aut_res = {get_roots(cursor)[i]:a[i, 0] for i in range(len(get_roots(cursor)))}

print(aut_res)

