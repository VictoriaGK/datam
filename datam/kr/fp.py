import math
import re
import zlib
import pymorphy2
morph = pymorphy2.MorphAnalyzer()



text = '''Во поле береза стояла,
Во поле кудрявая стояла,
Люли-люли, стояла,
Люли-люли, стояла.
Некому березу заломати,
Некому кудряву заломати,
Люли-люли, заломати,
Люли-люли, заломати.
Я ж пойду погуляю,
Белую березу заломаю,
Люли-люли, заломаю,
Люли-люли, заломаю.
Срежу я с березы три пруточка,
Сделаю из них я три гудочка,
Люли-люли, три гудочка,
Люли-люли, три гудочка.
Четвертую балалайку,
Четвертую балалайку,
Люли-люли, балалайку,
Люли-люли, балалайку.
Пойду на новые на сени,
Стану в балалаечку играти,
Люли-люли, играти,
Люли-люли, играти!'''

m = len(set(text))
n = math.ceil(7 * m / math.log(2))
k = math.floor(n/m * math.log(2))

def words(text):
    a = []
    b = text.split()
    for c in b:
        if (c is not None):
            c = re.findall(r'[а-я\w-]*', c.lower())
            c = morph.parse(c[0])[0].normal_form
            if (c != ''):
                a.append(c)
    return a



def bloom_filt(words):
    bfilt = [0] * n
    for word in words:
        crc = zlib.crc32(bytes(word.encode())) % n
        adl = zlib.adler32(bytes(word.encode())) % n
        bfilt[crc] += 1
        bfilt[adl] += 1
    return bfilt


print(words(text))
print(bloom_filt(words(text)))





